## Projekt Zespołowy 1 2020/2021 Grupa 3. Skuteczne odmontowywanie ##
### Skład grupy:
* Krzysztof Ziobro
* Dzianis Pivavarau
* Rafał Kilar
### Opis problemu:
Celem projektu jest przygotowanie skryptu umożliwiającego odmontowywanie zasobu w sytuacji w której jest on zajęty - korzystają z niego aktualnie pewne procesy, przez co nie jest możliwe odmontowanie w sposób standardowy.
### Rozwiązanie:
Rozwiązanie będzie korzystać z linuxowej komendy fuser służącej do wykrywania procesów korzystających z danego pliku.

Korzystamy z następujących flag fuser:

* flaga -m: sprawia że wykrywane są procesy nie tylko korzystające z konkretnego pliku, ale z całego zamontowanego systemu plików
* flaga -k: wysyła sygnał do wszystkich wykrytych procesów
* opcja -signal SIGTERM: modyfikuje wysyłany do procesów sygnał na SIGTERM, przez co może pozwolić procesom na poprawne zakończenie działania. Ten sygnał będziemy wysyłać jako pierwszy, w razie niepowodzenia zakończymy procesy używając domyślnego SIGKILL 
  
W ten sposób możemy zakończyć wszystkie procesy korzystające ze wskazanego zamontowanego systemu plików i następnie odmontować zasób który już nie jest zajęty.

### Problemy do rozwiązania:
* Dobór odpowiedniego opóźnienia między wysłaniem sygnałów SIGTERM i SIGKILL, takiego które umożliwia procesom poprawnie obsługującym SIGTERM możliwość poprawnego zakończenia się.
