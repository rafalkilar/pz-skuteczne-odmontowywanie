% EFFECTIVE-UMOUNT(1) effective-umount 1.0
% Rafal Kilar, Dzianis Pivavarau, Krzysztof Ziobro
% January 2021

# NAME
effective-umount - stop processees accessing mountpoint and unmount it

# SYNOPSIS
**effective-umount** mountpoint [*time*] [*attempts*]

# DESCRIPTION
**effevtive-umount** checks for processes accessing a given mountpoint and attempts to stop them by sending a SIGTERM signal. After *time* seconds (by default 1) it sends SIGKILL to any processes still accessing the resource. It continues sending SIGKILL until it successfully unmounts the resource or if it failes *attempts* times (by default 1).

# OPTIONS
**-h**, **\-\-help**
: Displays help message and exits.

# SEE ALSO
**umount**(8), **fuser**(1) 
