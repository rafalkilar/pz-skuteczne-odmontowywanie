#!/bin/bash

function no_processes {
	echo "No processes accesing given filesystem: Trying to unmount."
	if umount $1 ; then
		echo "Success."	
		exit
	fi
}

function attempt_umount {

uint_re='^[0-9]+$'
if ! [[ $2 =~ $uint_re ]] || ! [[ $3 =~ $uint_re ]] ; then
	   echo "One of the arguments should be a positive integer but is not.";
	   exit
fi


if [[ $(fuser -m $1 2> /dev/null) ]]; then	# check for processes using mounted filesystem
	echo "There are processes using filesystem you are trying to unmount: Sending SIGTERM."
	fuser -mk -SIGTERM $1 &> /dev/null
else
	no_processes $1	
fi

sleep $2

for it in `seq 1 $3`; do
	if [[ $(fuser -m $1 2> /dev/null) ]]; then
		echo "Some processes are still alive: Sending SIGKILL."
		fuser -mk -SIGKILL $1 &> /dev/null
	else
		no_processes $1
	fi
done

}

if [ "$#" -eq "0" ]; then
	echo "No arguments given. Terminating."
	exit
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
	echo "USAGE:"
	echo "effective-umount MOUNTPOINT TIME ATTEMPTS"
	echo "-----------------------------------------"
	echo "MOUNTPOINT - mountpoint of filesystem to be unmounted"
	echo "TIME - time (in seconds) for processes termination before sending SIGKILL. Default is 1."
	echo "ATTEMPTS - number of unmount attempts, setting it high might help if a lot of new processes are accessing MOUNTPOINT. Default is 1."
	exit
fi

if ! mountpoint -q -- "$1"; then
	echo "First argument is not a proper mountpoint name."
	exit
fi

case "$#" in
	"1") attempt_umount $1 1 1   ;;
	"2") attempt_umount $1 $2 1  ;;
	"3") attempt_umount $1 $2 $3 ;;
	*) echo "Incorrect number of arguments."
esac

